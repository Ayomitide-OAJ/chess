package com.bitbucket.ayomitideoaj.chess;

import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

import com.bitbucket.ayomitideoaj.chess.UtilityFragments.AboutFragment;
import com.bitbucket.ayomitideoaj.chess.UtilityFragments.SettingsFragment;
import com.bitbucket.ayomitideoaj.chess.UtilityFragments.UtilityFragment;

import com.bitbucket.ayomitideoaj.chess.R;
import com.yodo1.mas.Yodo1Mas;
import com.yodo1.mas.error.Yodo1MasError;
import com.yodo1.mas.helper.model.Yodo1MasAdBuildConfig;

public class MainActivity extends AppCompatActivity implements UtilityFragment.IUtilityFragment{

    private FrameLayout utilityFrame;
    private UtilityFragment utilityFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        utilityFrame = findViewById(R.id.utility_fragment_frame);
        utilityFrame.bringToFront();

        Yodo1MasAdBuildConfig config = new Yodo1MasAdBuildConfig.Builder().enableUserPrivacyDialog(true).build();
        Yodo1Mas.getInstance().setAdBuildConfig(config);

        Yodo1Mas.getInstance().init(this, "u8qc8z8hjy", new Yodo1Mas.InitListener() {
            @Override
            public void onMasInitSuccessful() {

            }

            @Override
            public void onMasInitFailed(@NonNull Yodo1MasError error) {

            }
        });


        Button startGameButton = findViewById(R.id.start_game_button);
        startGameButton.setOnClickListener(view -> {
            Intent startModeActivity = new Intent(getApplicationContext(), ModeActivity.class);
            startActivity(startModeActivity);
        });

        Button settingsButton = findViewById(R.id.settings_button);
        settingsButton.setOnClickListener(view -> {
            utilityFragment = new SettingsFragment();
            getFragmentManager().beginTransaction().add(
                    R.id.utility_fragment_frame, utilityFragment).commit();
            utilityFrame.setVisibility(View.VISIBLE);
        });

        Button aboutButton = findViewById(R.id.about_button);
        aboutButton.setOnClickListener(view -> {
            utilityFragment = new AboutFragment();
            getFragmentManager().beginTransaction().add(
                    R.id.utility_fragment_frame, utilityFragment).commit();
            utilityFrame.setVisibility(View.VISIBLE);
        });


    }

    @Override
    public void closeUtility() {
        utilityFrame.setVisibility(View.GONE);
        getFragmentManager().beginTransaction().remove(utilityFragment).commit();
    }
}
