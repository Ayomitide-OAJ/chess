package com.bitbucket.ayomitideoaj.chess;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;

import com.bitbucket.ayomitideoaj.chess.UtilityFragments.UtilityFragment;

public class ModeActivity extends AppCompatActivity implements UtilityFragment.IUtilityFragment {

    private FrameLayout utilityFrame;
    private FrameLayout fragmentFrame;
    private UtilityFragment utilityFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mode);

        fragmentFrame = findViewById(R.id.game_fragment_frame);
        CpuFragment cpuFragment = new CpuFragment();

        Button twoPlayerButton = findViewById(R.id.two_player_button);
        twoPlayerButton.setOnClickListener(view -> {
            Intent startGameActivity = new Intent(getApplicationContext(), GameActivity.class);
            startActivity(startGameActivity);
        });

        Button cpuButton = findViewById(R.id.cpu_button);
        cpuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSupportFragmentManager().beginTransaction().add(R.id.game_fragment_frame, cpuFragment).commit();
                fragmentFrame.bringToFront();
                fragmentFrame.setVisibility(View.VISIBLE);
            }
        });

    }

//    public void inflateFrame() {
//        getSupportFragmentManager().beginTransaction().add(R.id.game_fragment_frame, cpuFragment).commit();
//        fragmentFrame.bringToFront();
//        fragmentFrame.setVisibility(View.VISIBLE);
//    }

    @Override
    public void closeUtility() {
        utilityFrame.setVisibility(View.GONE);
        getFragmentManager().beginTransaction().remove(utilityFragment).commit();
    }
}