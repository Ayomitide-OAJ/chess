package com.bitbucket.ayomitideoaj.chess;

import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;


public class CpuFragment extends Fragment {
    private Fragment self;

    public CpuFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        self = this;
        View view = inflater.inflate(R.layout.fragment_cpu, container, false);

        Button backButton = view.findViewById(R.id.back_button);
        backButton.setOnClickListener(v -> {
            Intent startModeActivity = new Intent(getActivity(), ModeActivity.class);
            startActivity(startModeActivity);
        });

        return view;
    }
}